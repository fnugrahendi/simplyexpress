import express from 'express'
import { userRouter, memberRouter, billRouter } from './src/routes'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import path from 'path'

// load env variable
require('dotenv').load()

// constants and express object instance
const PORT = process.env.PORT ? process.env.PORT : 5000
const DB_URL = process.env.DB_URL ? process.env.DB_URL : 
"mongodb://<dbuser>:<dbpassword>@firstcluster-shard-00-00-p5o9a.gcp.mongodb.net:27017,firstcluster-shard-00-01-p5o9a.gcp.mongodb.net:27017,firstcluster-shard-00-02-p5o9a.gcp.mongodb.net:27017/test?ssl=true&replicaSet=FirstCluster-shard-0&authSource=admin&retryWrites=true"
const app = express()

// express app setup
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// static file
app.use(express.static(path.join(__dirname, 'client/build')))
// app.get('*', (req, res) => {
//   res.sendFile(path.join(__dirname+'/client/build/index.html'))
// })

// set routers
app.use('/api/users', userRouter)
app.use('/api/members', memberRouter)
app.use('/api/bills', billRouter)

// mongoose connect to DB
const mongoCallback = (error) => {
  return error ? console.log('mongodb error ', error) : console.log('mongodb connection success')
}
const mongoDB = mongoose.connect(DB_URL, { useNewUrlParser: true }, mongoCallback)

// express app listener
app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
})
