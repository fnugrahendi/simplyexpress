import mongoose, { Schema } from 'mongoose'

const MemberSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    rt: {
        type: String,
        required: true
    },
    meterNumber: {
        type: String,
    },
    _bills: [ { type: Schema.Types.ObjectId, ref: 'Bill' } ]
})

export default mongoose.model('Member', MemberSchema)