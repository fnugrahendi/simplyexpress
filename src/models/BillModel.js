import mongoose, { Schema } from 'mongoose'

const ObjectId = Schema.Types.ObjectId

const BillModel = new Schema({
    member: {
        type: ObjectId,
        ref: 'Member'
    },
    billTime: {
        type: Date,
        required: true,
        default: new Date(),
    },
    amount: {
        type: Number,
        required: true,
        default: 0,
    },
    paidTime: {
        type: Date,
        required: false,
        default: undefined,
    },
    personInCharge: {
        type: String,
        required: true,
        default: 'KarangTaruna',
    }
})

export default mongoose.model('Bill', BillModel)