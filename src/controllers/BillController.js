import StatusCode from '../constants/statusCode'
import mongoose from 'mongoose'
import Member from '../models/MemberModel'
import Bill from '../models/BillModel'

// Controller for Bill

// get all bills
export const getAllBills = (req, res) => {
    Bill.find()
        .populate('member')
        .exec((err, bills) => {
        return res.status(StatusCode.SUCCESS).send({
            success: 'true',
            message: 'bills retreived successfully',
            bills,
        })
    })
}

// add new bill
export const addBill = (req, res) => {
    const { billTime, amount, paidTime, personInCharge, memberId } = req.body
    const bill = new Bill({
        billTime,
        amount,
        paidTime,
        personInCharge,
        member: new mongoose.Types.ObjectId(memberId)
    })
    Member.findById(memberId, (err, member) => {
        member._bills.push(bill)
        member.save()
    })
    bill.save()
    return res.status(StatusCode.SUCCESS_POST).send({
        success: 'true',
        message: 'bill added successfully',
        bill,
    })
}

// get bill by memberId
export const findBillByMemberId = (req, res) => {
    let memberId = req.params.id
    if (!memberId) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid memberid: ${memberId}`,
        })
    }
    Bill.find({ _memberId: memberId }, (error, bill) => {
        if (!error && bill) {
            // query all bill under selected member
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'bills retreived successfully',
                bills: bill,
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `bill for memberId ${memberId} does not exist`,
                error: error
            })
        }
    })
}