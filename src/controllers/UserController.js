import StatusCode from '../constants/statusCode'
import { isValidUser } from '../helper/validation'
import User from '../models/UserModel'

// Controllers for User Model
// get all users from database
export const getAllUsers = (req, res) => {
    User.find({}, (err, users) => {
        return res.status(StatusCode.SUCCESS).send({
            success: 'true',
            message: 'user profile retreived successfully',
            users,
        })
    })
}

// add user
export const addUser = (req, res) => {
    let userModel = req.body
    let isUserValid = isValidUser(userModel)
    if (!isUserValid.isValid) {
        return res.status(StatusCode.ERROR).send({
            success: 'false',
            message: isUserValid.message,
        })
    } else {
        let user = new User(userModel)
        user.save()
        return res.status(StatusCode.SUCCESS_POST).send({
            success: 'true',
            message: 'new user has been successfully added',
            user,
        })
    }
}

// get user by specific id
export const getUserById = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid userid: ${req.params.id}`,
        })
    }
    User.findById(id, (error, user) => {
        if (!error && user) {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'user profile retreived successfully',
                user,
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `user id ${id} does not exist`,
            })
        }
    })
}

// update user
export const updateUser = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid userid: ${req.params.id}`,
        })
    }
    User.findByIdAndUpdate(id,  { $set: req.body }, (error, user) => {
        if (!error && user) {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'user profile updated successfully',
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `user profile failed to update`,
            })
        }
    })
}

// remove user
export const removeUser = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid userid: ${req.params.id}`,
        })
    }
    User.findByIdAndDelete(id, (error, user) => {
        if (!error && user) {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'user profile deleted successfully',
                user,
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `user profile failed to delete`,
            })
        }
    })
}