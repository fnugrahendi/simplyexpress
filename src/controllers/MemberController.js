import StatusCode from '../constants/statusCode'
import Member from '../models/MemberModel'
import Bill from '../models/BillModel'

// Controllers for Member Model
// get all members from database
export const getAllMembers = (req, res) => {
    Member.find()
        .populate('_bills')
        .exec((err, members) => {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'members retreived successfully',
                members,
            })    
        })
    // Member.find({}, (err, members) => {
    //     return res.status(StatusCode.SUCCESS).send({
    //         success: 'true',
    //         message: 'members retreived successfully',
    //         members,
    //     })
    // })
}

// add member
export const addMember = (req, res) => {
    let memberModel = req.body
    let member = new Member(memberModel)
    member.save()
    return res.status(StatusCode.SUCCESS_POST).send({
        success: 'true',
        message: 'new member has been successfully added',
        member,
    })
}

// get member by specific id
export const getMemberById = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid memberid: ${req.params.id}`,
        })
    }
    Member.findById(id)
        .populate('_bills')
        .exec((error, member) => {
            if (!error && member) {
                return res.status(StatusCode.SUCCESS).send({
                    success: 'true',
                    message: 'member retreived successfully',
                    member,
                })
            } else {
                return res.status(StatusCode.ERROR_NOT_FOUND).send({
                    success: 'false',
                    message: `member id ${id} does not exist`,
                })
            }
    })
}

// update member
export const updateMember = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid memberid: ${req.params.id}`,
        })
    }
    Member.findByIdAndUpdate(id, { $set: req.body }, (error, member) => {
        if (!error && member) {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'member profile updated successfully',
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `member profile failed to update`,
            })
        }
    })
}

// remove member
export const removeMember = (req, res) => {
    let id = req.params.id
    if (!id) {
        return res.status(StatusCode.ERROR_NOT_FOUND).send({
            success: 'false',
            message: `invalid memberid: ${req.params.id}`,
        })
    }
    Member.findByIdAndDelete(id, (error, member) => {
        if (!error && member) {
            return res.status(StatusCode.SUCCESS).send({
                success: 'true',
                message: 'member profile deleted successfully',
                member,
            })
        } else {
            return res.status(StatusCode.ERROR_NOT_FOUND).send({
                success: 'false',
                message: `member profile failed to delete`,
            })
        }
    })
}