// validate user model
export const isValidUser = ({ username, password }) => {
    if (!username || typeof (username) !== "string") {
        return {
            isValid: false,
            message: 'username must be string and cannot be empty'
        }
    } else if (!isValidPassword(password)) {
        return {
            isValid: false,
            message: 'password invalid'
        }
    }
    return {
        isValid: true
    }
}

// password validation rule
export const isValidPassword = (password) => {
    return (password && password !== '')
}