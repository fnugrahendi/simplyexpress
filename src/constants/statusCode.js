const StatusCode = {
    ERROR_NOT_FOUND: 404,
    ERROR: 400,
    SUCCESS: 200,
    SUCCESS_POST: 201,
    INTERNAL_SERVER_ERROR: 500,
}

export default StatusCode