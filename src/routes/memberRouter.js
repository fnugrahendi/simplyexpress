import { Router } from 'express'
import * as memberController from '../controllers/MemberController'

// router object instance
const memberRouter = Router()

// Member routes
memberRouter.get('/', memberController.getAllMembers)
memberRouter.get('/:id', memberController.getMemberById)
memberRouter.post('/add', memberController.addMember)
memberRouter.delete('/:id', memberController.removeMember)
memberRouter.put('/:id', memberController.updateMember)

export default memberRouter