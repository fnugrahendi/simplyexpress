import { Router } from 'express'
import * as userController from '../controllers/UserController'

// router object instance
const userRouter = Router()

// User routes
userRouter.get('/', userController.getAllUsers)
userRouter.get('/:id', userController.getUserById)
userRouter.post('/add', userController.addUser)
userRouter.delete('/:id', userController.removeUser)
userRouter.put('/:id', userController.updateUser)

export default userRouter