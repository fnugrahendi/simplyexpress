import { Router } from 'express'
import * as billController from '../controllers/BillController'

const billRouter = Router()

billRouter.post('/add', billController.addBill)
billRouter.get('/', billController.getAllBills)
billRouter.get('/member/:id', billController.findBillByMemberId)

export default billRouter