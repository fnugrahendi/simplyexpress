import React, { useEffect, useState, useReducer, useContext, createContext } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'
import Members from './Members'
import './../styles/App.scss';

const MemberBillContext = createContext(null)

const initialMemberBill = []

const actionTypes = {
  ADD_MEMBER: "ADD_MEMBER",
  UPDATE_MEMBER: "UPDATE_MEMBER",
  SET_MEMBER: "SET_MEMBER",
  UPDATE_BILL_STATUS: "UPDATE_BILL_STATUS",
}

const memberBillReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.ADD_MEMBER:
      return [...state, action.member]
    case actionTypes.SET_MEMBER:
      return action.members
    case actionTypes.UPDATE_MEMBER:
      return state.map(member => {
        if (member.id === action.id) {
          return action.member
        } else {
          return member
        }
      })
    case actionTypes.UPDATE_BILL_STATUS:
      return state.map(member => {
        if (member.id === action.id) {
          return { ...member, bill: action.bill }
        } else {
          return member
        }
      })
    default:
      break
  }
}

const fetchApi = (param) => {
  return fetch(`api/${param}`)
}

function App() {
  const [member, dispatchMember] = useReducer(memberBillReducer, initialMemberBill)

  const setMember = (val) => {
    dispatchMember({
      type: actionTypes.SET_MEMBER,
      members: val
    })
  }

  useEffect(() => {
    fetchApi('members')
      .then(resp => {
        return resp.json()
      })
      .then(data => {
        setMember(data)
        console.log(data)
      })
    console.log(member)
  }, [member])

  return (
    <MemberBillContext.Provider value={dispatchMember}>
      <div className="App">
        <Router>
          <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#">SimplyExpress</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <Link className="nav-item nav-link" to="/members">Warga</Link>
                <Link className="nav-item nav-link" to="/users">Petugas</Link>
              </div>
            </div>
          </nav>
          <div>
            <Route path="/members" component={Members} />
            <Route path="/users" component={Members} />
          </div>
        </Router>
      </div>
    </MemberBillContext.Provider>
  )
}



export default App;
